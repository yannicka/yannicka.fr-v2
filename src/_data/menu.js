module.exports = () => {
  return [
    {
      name: 'Journal',
      path: '/journal/',
    },

    {
      name: 'Projets',
      path: '/projets/',
    },

    {
      name: 'Exercices',
      path: '/exercices/',
    },
  ]
}
